# Minimal Broach

This is an attempt to design a very simple rotary broach holder
that can be made with only a mill. The broach is oiled or greased
and rotates in the body of the holder which acts as a bushing.
This design uses a pair of ball bearings as a thrust bearing.
It could also work with a single bearing by drilling the inner hole
6mm shorter.

This design is intended only for light, occasional use.  It is
incompatible with pressure-relieved broaches because the hole would
bearon the ball bearing. It has a fixed focus and works with only
one length of broach; here, 28mm. It requires 2 6mm ball bearings
as designed; could be made with smaller bearings.

As of this writing, this design is in development.

## Alternatives

* Fixed-focus broach holders
  * [Hemingway Compact Rotary Broach](https://www.hemingwaykits.com/HK2570)
  * [My alternative to the Hemingway design](https://gitlab.com/mcdanlj/RotaryBroach)
    intended to be used as a variant after purchasing the Hemingway kit
* [Floating-focus broach holder](https://gitlab.com/mcdanlj/FloatingRotaryBroach) that can work with a variety of lengths of broaches without adjustment.
